mod brainrust;

extern crate getopts;

use getopts::Options;
use std::env;
use std::io::Read;
use std::fs::File;
use brainrust::lexer::Lexer;
use brainrust::token::TokenType;

fn print_version() {
    const VERSION: &'static str = env!("CARGO_PKG_VERSION");
    println!("brainrust Version {}", VERSION);
    println!("Copyright (C) 2015, Felix Bytow <felix.bytow@googlemail.com>");
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options] FILENAME", program);
    println!("{}", opts.usage(&brief));
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optflag("h", "help", "show this help");
    opts.optflag("v", "version", "show version information");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }

    if matches.opt_present("v") {
        print_version();
        return;
    }

    let filename = if !matches.free.is_empty() {
        matches.free[0].clone()
    }
    else {
        print_usage(&program, opts);
        return;
    };

    let input = match File::open(filename.clone()) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };

    let mut lexer = Lexer::new(filename, input.bytes());
    loop {
        let tok = lexer.next_token();
        println!("{}", tok);
        match tok.which {
            TokenType::Eof => break,
            _ => {},
        }
    }
}
