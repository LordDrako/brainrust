use std::fmt;

#[derive(Debug)]
pub enum TokenType {
    Plus,
    Minus,
    GreaterThan,
    LessThan,
    BracketLeft,
    BracketRight,
    Dot,
    Comma,
    Eof,
}

pub struct Token<'a> {
    pub which:    TokenType,
    pub row:      u32,
    pub column:   u32,
    pub filename: &'a String,
}

impl<'a> fmt::Display for Token<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(File: {}, Row: {}, Column: {}, Token: {:?})", self.filename, self.row, self.column, self.which)
    }
}
