use std::io::Bytes;
use std::fs::File;
use brainrust::token::*;

pub struct Lexer {
    filename: String,
    bytes: Bytes<File>,
    row: u32,
    column: u32,
}

impl Lexer {
    pub fn new(filename: String, bytes: Bytes<File>) -> Lexer {
        return Lexer {
            filename: filename,
            bytes: bytes,
            row: 0u32,
            column: 0u32,
        };
    }

    pub fn filename(&self) -> String {
        return self.filename.clone();
    }

    pub fn next_token(&mut self) -> Token {
        let mut tok = Token {
            which: TokenType::Eof,
            row: self.row,
            column: self.column,
            filename: &self.filename,
        };

        // the loop is used to skip whitespace
        // and non bf characters
        // EOF or a valid bf character will make the function return
        loop {
            let c: u8 = match self.bytes.next() {
                Some(m) => { m.unwrap() }
                None => { return tok; }
            };

            self.column = self.column + 1;
            tok = Token { column: self.column, .. tok };

            match c as char {
                '\n' => { self.column = 0; self.row = self.row + 1; continue; }
                '+' => { tok.which = TokenType::Plus; }
                '-' => { tok.which = TokenType::Minus; }
                '>' => { tok.which = TokenType::GreaterThan; }
                '<' => { tok.which = TokenType::LessThan; }
                '[' => { tok.which = TokenType::BracketLeft; }
                ']' => { tok.which = TokenType::BracketRight; }
                '.' => { tok.which = TokenType::Dot; }
                ',' => { tok.which = TokenType::Comma; }
                _ => { continue; }
            };

            break;
        }

        return tok;
    }
}
